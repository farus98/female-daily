import express from 'express';
import application from './application';

const app = express();

app.use('/femaledaily', application);

app.listen(3000,()=> console.log('server run in port 3000'));
