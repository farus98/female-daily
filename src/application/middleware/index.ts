import makeTwitter from './twitter'

const twitterApi = makeTwitter({})

const femaleDailyMiddleware = Object.freeze({
    twitterApi
})

export default femaleDailyMiddleware

export {
    twitterApi
}
