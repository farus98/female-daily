import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import {ErrorHandler, handleError} from './helpers'
var path = require('path');
const app = express();

const makeCallback = require ('./call-back')

import {
    getTransaction,
    getTwitter
} from "./controller"

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(cors());
app.use(bodyParser.json());


app.get('/transaction', async (req,res)=>{
    const getData = await getTransaction({query:req.query})
    const dataT = getData.body.data
    console.log('data', dataT);
    
    res.render('transaction',{listData: dataT}); 
});

app.get('/pivot', async (req,res)=>{    
    const getData = await getTransaction({query: {button: "pivot"}})
    const dataT = getData.body.data
    res.render('transaction-pivot',{listData: dataT}); 
});

// app.get('/transaction', makeCallback(getTransaction,camelCaseKeys))
app.get('/twitter', makeCallback(getTwitter));

app.use((err:any, req:any, res:any, next:any) => {
    handleError(err, res);
});

export default app;
