import { dataDb } from "../data-access"
import { twitterApi } from "../middleware"

import makeGetDataTransaction from "./get-data-transaction"
import makeGetDataTwitter from "./get-data-twitter"

const getDataTransaction = makeGetDataTransaction({dataDb})
const getDataTwitter = makeGetDataTwitter({twitterApi})

const femaleDailyService = Object.freeze({
    getDataTransaction, 
    getDataTwitter
})

export default femaleDailyService
export {
   getDataTransaction, 
   getDataTwitter
}