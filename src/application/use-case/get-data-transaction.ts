export default function makeGetDataTransaction({dataDb}) {
  return async function getDataTransaction(body:any) {
    try {
      
      const getData =  await dataDb.showDataTransaction(body)
      
      let result

      if(getData.length > 0){
        let information
        if(body.button == "pivot"){
          information = "Data Pivot"
        }else{
          information = "Data Found"
        }
        result = {responseCode: 200, information: information, data: getData}
      }else{
        result = {responseCode: 204, information: "Data Not Found", data: []}
      }
    
      return result

    } catch (error:any) {
      throw new Error(error);
    }
  }   
}  
