export default function makeCreateTransaction({dataDb}) {
  return async function createTransaction(body:any) {
    try {
    
      let result

      const insertTransaction = await dataDb.createDataTransaction(body)

      if(insertTransaction > 0){      
        result = {responseCode: 201, information: "Your Transaction is Successfully Recorded"}
      }else{
        result = {responseCode: 400, information: "Your Transaction Failed Recorded"}
      }

      return result
  
    } catch (error:any) {
      throw new Error(error);
    }
  
  }    
}  