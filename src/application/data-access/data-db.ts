export default function makeDataDb({Query}) {
    return Object.freeze({
      showDataTransaction, 
      createDataTransaction
    })

    async function showDataTransaction(body){
      return new Promise(async function(resolve, reject) {
        try{
          
          let sql 

          if(body.button == "pivot"){
          
            sql = `SELECT concat(firstname,' ',lastname) as "fullname", email, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang1') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang1') end barang1, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang2') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang2') end barang2, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang3') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang3') end barang3, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang4') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang4') end barang4, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang5') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang5') end barang5, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang6') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang6') end barang6, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang7') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang7') end barang7, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang8') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang8') end barang8, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang9') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang9') end barang9, 
                      case when sum(quantity) FILTER (WHERE item = 'Barang10') is null then 0 else sum(quantity) FILTER (WHERE item = 'Barang10') end barang10
                    FROM public.transaction 
                    GROUP BY concat(firstname,' ',lastname),email`
          }else{
            
            sql = `SELECT id, concat(firstname,' ',lastname) as "fullname", email, item, quantity, total_price as "totalPrice" 
                    FROM transaction ORDER BY id ASC`;
          }

          let result = await Query(sql);
  
          resolve(result.rows)
        
        } catch(error){
          reject(new Error('dataDb-showDataTransaction '+error));
        }
      })
    }

    async function createDataTransaction(body){
      return new Promise(async function(resolve, reject) {
        try{
          
          let sql = `INSERT INTO transaction (firstname, lastname, email, item, quantity, total_price)
                      VALUES ('${body.firstname}', '${body.lastname}', '${body.email}', '${body.item}', '${body.quantity}', '${body.totalPrice}');`;
          
          let result = await Query(sql);
  
          resolve(result.rowCount)
        
        } catch(error){
          reject(new Error('transactionDb-createDataTransaction '+error));
        }
      })
    }

    

}
  