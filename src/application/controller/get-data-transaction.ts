export default function makeGetTransaction({getDataTransaction}) {
  return async function getTransaction(httpRequest) {
    try {
      const bodyparam  = httpRequest.query

      const posted = await getDataTransaction(bodyparam)
      // console.log('posted', posted);
      
      return {
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
        body: {
          status : true,
          ...posted
        }, 
        info: 'view html'
      }
    } catch (err:any) {
      console.log(err);

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          status : false,
          response_code : 400,
          message: err.message
        }
      }
    }
  }
}
