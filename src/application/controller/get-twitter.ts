export default function makeGetTwitter({getDataTwitter}) {
  return async function getTwitter(httpRequest) {
    try {
      const bodyparam  = httpRequest.query

      const posted = await getDataTwitter(bodyparam)

      return {
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
        body: {
          status : true,
          ...posted,
          info: 'no'
        }
      }
    } catch (err:any) {
      console.log(err);

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          status : false,
          response_code : 400,
          message: err.message
        }
      }
    }
  }   
} 