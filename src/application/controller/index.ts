import {
    getDataTransaction, 
    getDataTwitter
} from "../use-case"

import makeGetTransaction from "./get-data-transaction"
import makeGetTwitter from "./get-twitter"

const getTransaction = makeGetTransaction({getDataTransaction})
const getTwitter = makeGetTwitter({getDataTwitter})

const femaleDailyController = Object.freeze({
    getTransaction, 
    getTwitter
})

export default femaleDailyController
export {
   getTransaction, 
   getTwitter
}