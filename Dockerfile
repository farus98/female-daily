FROM node:12

RUN mkdir /home/node/app
WORKDIR /home/node/app
COPY package.json package.json
RUN npm install
COPY src src
WORKDIR dist
EXPOSE 3000
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
CMD ["node","app.js"]