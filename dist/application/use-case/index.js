"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDataTwitter = exports.getDataTransaction = void 0;
const data_access_1 = require("../data-access");
const middleware_1 = require("../middleware");
const get_data_transaction_1 = __importDefault(require("./get-data-transaction"));
const get_data_twitter_1 = __importDefault(require("./get-data-twitter"));
const getDataTransaction = (0, get_data_transaction_1.default)({ dataDb: data_access_1.dataDb });
exports.getDataTransaction = getDataTransaction;
const getDataTwitter = (0, get_data_twitter_1.default)({ twitterApi: middleware_1.twitterApi });
exports.getDataTwitter = getDataTwitter;
const femaleDailyService = Object.freeze({
    getDataTransaction,
    getDataTwitter
});
exports.default = femaleDailyService;
