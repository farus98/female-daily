"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTwitter = exports.getTransaction = void 0;
const use_case_1 = require("../use-case");
const get_data_transaction_1 = __importDefault(require("./get-data-transaction"));
const get_twitter_1 = __importDefault(require("./get-twitter"));
const getTransaction = (0, get_data_transaction_1.default)({ getDataTransaction: use_case_1.getDataTransaction });
exports.getTransaction = getTransaction;
const getTwitter = (0, get_twitter_1.default)({ getDataTwitter: use_case_1.getDataTwitter });
exports.getTwitter = getTwitter;
const femaleDailyController = Object.freeze({
    getTransaction,
    getTwitter
});
exports.default = femaleDailyController;
