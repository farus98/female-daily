"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const helpers_1 = require("./helpers");
var path = require('path');
const app = (0, express_1.default)();
const makeCallback = require('./call-back');
const controller_1 = require("./controller");
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use((0, cors_1.default)());
app.use(body_parser_1.default.json());
app.get('/transaction', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const getData = yield (0, controller_1.getTransaction)({ query: req.query });
    const dataT = getData.body.data;
    // console.log('data', dataT);
    res.render('transaction', { listData: dataT });
}));
app.get('/pivot', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const getData = yield (0, controller_1.getTransaction)({ query: { button: "pivot" } });
    const dataT = getData.body.data;
    res.render('transaction-pivot', { listData: dataT });
}));
// app.get('/transaction', makeCallback(getTransaction,camelCaseKeys))
app.get('/twitter', makeCallback(controller_1.getTwitter));
app.use((err, req, res, next) => {
    (0, helpers_1.handleError)(err, res);
});
exports.default = app;
