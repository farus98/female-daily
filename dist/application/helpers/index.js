"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleError = exports.ErrorHandler = void 0;
const error_1 = require("./error");
Object.defineProperty(exports, "ErrorHandler", { enumerable: true, get: function () { return error_1.ErrorHandler; } });
Object.defineProperty(exports, "handleError", { enumerable: true, get: function () { return error_1.handleError; } });
const helperServices = Object.freeze({
    ErrorHandler: error_1.ErrorHandler, handleError: error_1.handleError
});
exports.default = helperServices;
