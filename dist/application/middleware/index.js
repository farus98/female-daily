"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.twitterApi = void 0;
const twitter_1 = __importDefault(require("./twitter"));
const twitterApi = (0, twitter_1.default)({});
exports.twitterApi = twitterApi;
const femaleDailyMiddleware = Object.freeze({
    twitterApi
});
exports.default = femaleDailyMiddleware;
