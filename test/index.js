const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../dist/application/index')

chai.use(chaiHttp);
chai.should();

describe("Get Data Transaction", () => {
  it("Should Get All Transaction", (done) => {
    chai.request(app.default)
      .get('/transaction')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  });

  it("Should Get Transaction With Pivot", (done) => {
    chai.request(app.default)
      .get('/transaction?button=pivot')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  });

});