# INSTRUCTIONS RUN APPLICATION
------------------------------
   1. npm i
   2. npm run start 
   3. open browser and type the following link: http://localhost:3000/femaledaily/transaction

# INSTRUCTIONS RUN AUTOMATION TEST
----------------------------------
    npm run test

# INSTRUCTIONS RUN APPLICATION TYPESCRIPT
------------------------------
   1. npm i
   2. npm run dev
   3. open browser and type the following link: http://localhost:3000/femaledaily/transaction   