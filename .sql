CREATE TABLE IF NOT EXISTS public.transaction
(
    id integer NOT NULL DEFAULT nextval('transaction_id_seq'::regclass),
    firstname character varying COLLATE pg_catalog."default" NOT NULL,
    lastname character varying COLLATE pg_catalog."default" NOT NULL,
    email character varying COLLATE pg_catalog."default" NOT NULL,
    item character varying COLLATE pg_catalog."default" NOT NULL,
    quantity integer NOT NULL,
    total_price numeric NOT NULL,
    CONSTRAINT transaction_pkey PRIMARY KEY (id)
)

INSERT INTO public.transaction(firstname, lastname, email, item, quantity, total_price)
VALUES 
('fauzan','rusdi','fauzanrusdi20@gmail.com','Barang1','10',100000),
('suhartono','hartono','hartono@gmail.com','Barang1','5',50000),
('nurina','fildzah','fildzah@gmail.com','Barang2','5',45000),
('fitri','nurafifah','afitrinur@gmail.com','Barang3','7',25000),
('sharah','fadilah','sharahcans@gmail.com','Barang2','2',10000),
('diana','hertiyas','moody@gmail.com','Barang4','12',300000),
('joko','susilo','jok@gmail.com','Barang5','2',5000),
('rissa','aliria','rissa@gmail.com','Barang6','4',25000),
('syifa','amalia','syifa@gmail.com','Barang7','2',10000),
('muhamad','farhan','farhantv@gmail.com','Barang8','4',34000),
('shalzaviera','aznitesa','caca@gmail.com','Barang9','1',60000),
('dandi','stiyawan','danss@gmail.com','Barang9','9',55000),
('ingkan','putri','ingkan@gmail.com','Barang10','4',70000);